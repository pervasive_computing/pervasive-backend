FROM python:3.9-alpine
RUN apk add --no-cache build-base postgresql-dev

RUN pip install --upgrade pip

RUN adduser -D nonroot
RUN install -d /app -o nonroot -g nonroot
USER nonroot
WORKDIR /app

COPY --chown=nonroot:nonroot requirements.txt requirements.txt
RUN pip install --no-cache-dir --upgrade --user -r requirements.txt

ENV PATH="/home/nonroot/.local/bin:${PATH}"

ARG CACHE_DATE=2017-01-11

CMD ["sh", "./docker-entrypoint.sh" ]
