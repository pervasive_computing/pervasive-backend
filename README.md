## Prerequisites

#### With Docker

- docker
- docker-compose

#### Dev env without docker

- python3

## Getting started

### Dev environment with docker

0. Check values in .env.dev
2. Run `docker-compose up --build`

### Dev env without docker

0. Create virtual environment `python3 -m venv venv`
1. Activate the python environment. `. venv/bin/activate` on Linux/Mac OS, `venv\Scripts\activate` on Windows.
2. Copy the `.env.dev` file into your own `.env` file, for instance `cp .env.dev .env`. The default values are good for local development.
3. Run the server using `flask run`.

### Prod environment

1. Correct the values in .env.prod
2. To run in production there is two docker-compose files
  - Run with nginx proxy and letsencrypt certs `docker-compose -f docker-compose.yml  -f docker-compose.nginx.yml up`
  - To run with existing nginx-proxy use `docker-compose -f docker-compose.yml  -f docker-compose.without-nginx.yml up` (Check that network is correct in without-nginx file)
3. Stopping same way, define files eg. `docker-compose -f docker-compose.yml -f docker-compose.nginx.yml  down`

## More info

The following was used to create the project:

https://flask.palletsprojects.com/en/2.0.x/installation/

https://flask.palletsprojects.com/en/2.0.x/quickstart/#
