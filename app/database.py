from flask_sqlalchemy import SQLAlchemy
from enum import Enum
from dataclasses import dataclass
from datetime import datetime

#
# Database
#

db = SQLAlchemy()


#
# Init
#

if __name__ == "__main__":
  db.create_all()

#
# Models
#

class RoleEnum(str, Enum):
  HOSPITAL = "hospital"
  AMBULANCE = "ambulance"

@dataclass
class Users(db.Model):
  id: int
  name: str
  role: str

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(100))
  role = db.Column(db.Enum(RoleEnum))

  # Needed to create users using the constructor
  def __init__(self, id, name, role) -> None:
    self.id = id
    self.name = name
    self.role = role
    super().__init__()

@dataclass
class Item(db.Model):
  id: str
  name: str
  expiration_date: datetime
  type_id: int
  backpack_id: int
  count: int = 1

  id = db.Column(db.String(100), primary_key=True)
  name = db.Column(db.String(100))
  expiration_date = db.Column(db.DateTime)
  type_id = db.Column(db.String(100), db.ForeignKey('item_type.id'))
  backpack_id = db.Column(db.String(100), db.ForeignKey('backpack.id'))
  count = db.Column(db.Integer())

  def __init__(self, id, name=None, expiration_date=None, type_id=None, backpack_id=None, count = 1) -> None:
    self.id = id
    self.name = name
    self.expiration_date = expiration_date
    self.type_id = type_id
    self.backpack_id = backpack_id
    self.count = count
    super().__init__()

@dataclass
class Backpack(db.Model):
  id: int
  name: str
  items: list

  id = db.Column(db.String(100), primary_key=True)
  name = db.Column(db.String(100))
  items = db.relationship('Item', cascade='all,delete', backref='backpack', lazy=True)

  def __init__(self, id, name=None, items=None) -> None:
    self.id = id
    self.name = name
    self.items = items
    super().__init__()

@dataclass
class ItemType(db.Model):
  id: int
  name: str
  count: int = 1

  id = db.Column(db.String(100), primary_key=True)
  name = db.Column(db.String(100))
  count = db.Column(db.Integer())

  def __init__(self, id, name, count = 1) -> None:
    self.id = id
    self.name = name
    self.count = count
    super().__init__()

