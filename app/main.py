import eventlet
import logging
from flask import Flask, request, jsonify, json
from flask_cors import CORS
from flask_socketio import SocketIO
from dotenv import load_dotenv
from flask_mqtt import Mqtt
import os
from datetime import datetime
from sqlalchemy import func
from database import Backpack, Item, ItemType, RoleEnum, db, Users
from flasgger import Swagger


#http://eventlet.net/doc/patching.html#monkeypatching-the-standard-library
eventlet.monkey_patch()

load_dotenv()

ORIGINS = os.environ["ORIGINS"].split(",")
MQTT_BROKER_URL = os.environ["MQTT_BROKER_URL"]
MQTT_BROKER_PORT = os.environ["MQTT_BROKER_PORT"]
MQTT_TOPIC = os.environ["MQTT_TOPIC"]
FLASK_RUN_HOST = os.environ["FLASK_RUN_HOST"]
FLASK_RUN_PORT = os.environ["FLASK_RUN_PORT"]
USE_RELOADER = os.environ["USE_RELOADER"]

app = Flask(__name__)

# Mqtt stuff
app.config['MQTT_BROKER_URL'] = MQTT_BROKER_URL
app.config['MQTT_BROKER_PORT'] = int(MQTT_BROKER_PORT)
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
app.config['JSONIFY_MIMETYPE'] = "application/ld+json"

## Database stuff
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["DATABASE_URL"]
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# app.config['SQLALCHEMY_POOL_SIZE'] = 20
db.init_app(app)

mqtt = Mqtt(app)

swagger_config = {
    "headers": [
    #    ["Content-Type", 'text/html'],
    ],
    "specs": [
        {
            "endpoint": 'apispec_1',
            "route": '/apispec_1.json',
            "rule_filter": lambda rule: True,  # all in
            "model_filter": lambda tag: True,  # all in
        }
    ],
    "static_url_path": "/flasgger_static",
    # "static_folder": "static",  # must be set by user
    "swagger_ui": True,
    "specs_route": "/apidocs/"
}

swagger = Swagger(app, config=swagger_config)

## Routes
CORS(app)
socketio = SocketIO(app, cors_allowed_origins=ORIGINS, logger=True, engineio_logger=True)

build_time = datetime.utcnow()
lamp_status = False

#Custom response, set content-type to "application/ld+json"
# class WOTResponse(app.response_class):
#     def __init__(self, *args, **kwargs):
#         super(WOTResponse, self).__init__(*args, **kwargs)
#         self.content_type = "application/ld+json"

# app.response_class = WOTResponse

def create_item(item_dict):
    app.logger.info("creating item")
    name = item_dict.get("name", None)
    expiration_date = item_dict.get("expiration_date", None)
    backpack_id = item_dict.get("backpack_id", None)
    type_id = item_dict.get("type_id", None)
    id = item_dict.get("id", None)
    item = Item(id=id, name=name, expiration_date=expiration_date, type_id=type_id, backpack_id=backpack_id)
    db.session.add(item)
    db.session.commit()
    return item

def update_item(item_dict):
    app.logger.info("updating item")
    item = Item.query.filter_by(id=item_dict["id"]).first()#_or_404()
    if item == None:
        item = create_item(item_dict)
    else:
        item.name = item_dict.get("name", item.name)
        item.expiration_date = item_dict.get("expiration_date", item.expiration_date)
        item.count = item_dict.get("count", item.count)
        item.backpack_id = item_dict.get("backpack_id", item.backpack_id)
        item.type_id = item_dict.get("type_id", item.type_id)
        db.session.commit()
    return item

def create_backpack(backpack_dict):
    name = backpack_dict.get("name", None)
    items = backpack_dict.get("items", list())
    items_list = list()
    for item in items:
        i = create_item(item)
        items_list.append(i)
    id = backpack_dict.get("id", None)
    backpack = Backpack(id=id, name=name, items=items_list)
    db.session.add(backpack)
    db.session.commit()
    return backpack

# must come from mqtt {'id': XXXX, 'items': [{'id': YYYY, 'backpack_id': XXXX}, {'id': YYZZ, 'backpack_id': XXXX}]}
# We should change json also to wot specs?
def update_backpack(backpack_dict):
    with app.app_context():
        emit = False
        bp_id = str(backpack_dict["id"])
        backpack = Backpack.query.filter_by(id=bp_id).first()#_or_404()
        if backpack == None:
            app.logger.info("creating backpack")
            backpack = create_backpack(backpack_dict)
            emit = True
        backpack_asdict = json.loads(json.dumps(backpack), object_hook=date_hook)
        dict_item_ids = [msg_item['id'] for msg_item in backpack_dict['items']]
        item_ids = [msg_item['id'] for msg_item in backpack_asdict['items'] if msg_item['count'] != 0]
        has_ids = [item_id in dict_item_ids for item_id in item_ids]#backpack_asdict['items']]
        if not all(has_ids) or not has_ids:
            app.logger.info("updating backpack")
            #Backpack(json) has some new items, change its count to 1
            new_item_list = [{**x, 'count': 1} for x in backpack_dict['items'] if x['id'] not in item_ids]
            #Backpack(json) doesn't have some item, change its count to 0
            remove_item_list = [{**x, 'count': 0} for x in backpack_asdict['items'] if x['id'] not in dict_item_ids]
            for item in new_item_list+remove_item_list:
              update_item(item)
            emit = True
        if backpack.name != backpack_dict.get("name", backpack.name):
            backpack.name = backpack_dict.get("name", backpack.name)
            emit = True
        #backpack.items = backpack_dict.get("items", backpack.get("items"))
        db.session.commit()
        if emit:
            emit_backpack(backpack.id)
    return backpack

def get_backpack(id):
    backpack = Backpack.query.filter_by(id=id).first_or_404()
    # Serialize to load all information
    backpack = json.loads(json.dumps(backpack), object_hook=date_hook)
    item_types = json.loads(json.dumps(ItemType.query.all()))

    backpack = prepare_backpack(backpack)

    for i_type in item_types:
        i_type['items'] = [item for item in backpack['items'] if item['type_id'] == i_type['id']]
    backpack['item_types'] = item_types
    return backpack

def date_hook(json_dict):
    for (key, value) in json_dict.items():
        try:
            json_dict[key] = datetime.strptime(value, '%a, %d %b %Y %H:%M:%S GMT')
        except:
            pass
    return json_dict

def emit_backpack(bp_id: str):
    app.logger.info("emitting " + bp_id)
    backpack = get_backpack(bp_id)
    socketio.emit('backpacks/' + bp_id, json.dumps(backpack))

def prepare_backpack(backpack):
    for it in backpack['items']:
        #app.logger.info(it)
        it['expired'] = it['expiration_date'] is not None and it['expiration_date'] < datetime.utcnow()

    backpack['expiredItems'] = sum(it['expired'] for it in backpack['items'])

    return backpack

@app.route("/")
def hello_world():
    """Test route as a health check. Will sent an MQTT message as well.
    ---
    responses:
      200:
        description: A greeting and the last build time.
    """
    mqtt.publish(MQTT_TOPIC, '{"id": "0", "items": [{"id": "0", "backpack_id": "0"}, {"id": "88", "backpack_id": "0"}]}')
    return "<p>Hello Chris and Eetu and Lorenzo!!</p><p>Last build: {} UTC</p>".format(
        build_time
    )

@app.route("/api/login", methods=['POST'])
def login():
    """Route to login
    ---
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#definitions/LoginUser'
    responses:
      '200':
        description: Logged in
      '401':
        description: Invalid credentials
    definitions:
        LoginUser:
            type: object
            required:
                - name
            properties:
                name:
                    type: string
    """
    app.logger.info(request.json)
    # app.logger.info(request.data[])
    user = Users.query.filter(func.lower(Users.name).ilike(func.lower(request.json['username']))).first()
    if user is None:
        return ("Invalid credentials", 401)
    return jsonify(user)

### Users
###

@app.route("/api/users/create")
def create_user():
    """Test route to create a user
    ---
    responses:
      '204':
        description: User created
    """
    me = Users(1, 'Jore', RoleEnum.AMBULANCE)
    db.session.add(me)
    db.session.commit()
    return ("", 204)

@app.route("/api/users")
def show_all():
    """Route to list all users
    ---
    responses:
      '200':
        description: A list of users
        schema:
            type: array
            items:
                $ref: '#/definitions/User'
    definitions:
        User:
            type: object
            required:
                - id
                - name
                - role
            properties:
                id:
                    type: integer
                    example: 1337
                name:
                    type: string
                    example: "Jore Booy"
                role:
                    type: string
                    example: "ambulance"
    """
    users = Users.query.all()
    return jsonify(users)

@app.route('/api/users/<id>')
def show_user(id):
    """Get a specific user
    ---
    parameters:
      - name: id
        in: path
        type: integer
        required: true
    responses:
      '200':
        description: A single user
        schema:
            $ref: '#/definitions/User'
      '404':
        description: User not found
    """
    user = Users.query.filter_by(id=id).first_or_404()
    return jsonify(user)

#### Backpacks
####

@app.route("/api/backpacks")
def show_all_backpacks():
    """Route to list all backpacks
    ---
    responses:
      '200':
        description: A list of backpacks
        schema:
            type: array
            items:
                $ref: '#/definitions/Backpack'
    definitions:
        Backpack:
            type: object
            required:
                - id
                - name
                - items
            properties:
                id:
                    type: integer
                    example: 1337
                name:
                    type: string
                    example: "Backpack 1"
                items:
                    type: array
                    items:
                        $ref: '#/definitions/Item'
    """
    backpacks = Backpack.query.all()
    # Serialize to load all information
    backpacks = json.loads(json.dumps(backpacks), object_hook=date_hook)
    # Add expired item count
    for backpack in backpacks:
        prepare_backpack(backpack)

    return jsonify(backpacks)

@app.route("/api/backpacks/<id>")
def show_backpack(id):
    """Get a specific backpack
    ---
    parameters:
      - name: id
        in: path
        type: integer
        required: true
    responses:
      '200':
        description: A single backpack
        schema:
            $ref: '#/definitions/Backpack'
      '404':
        description: Backpack not found
    """
    return get_backpack(id)

@app.route("/api/backpacks/<id>/update", methods=['PATCH'])
def update_backpack_api(id):
    """Update a specific backpack and its items
    ---
    parameters:
      - name: id
        in: path
        type: integer
        required: true
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#definitions/Backpack'
    responses:
      '200':
        description: A single backpack
        schema:
            $ref: '#/definitions/Backpack'
      '404':
        description: Backpack not found
    """
    if request.method == "PATCH":
        backpack = request.get_json()
        backpack['id'] = id
        result = jsonify(update_backpack(backpack))
        #emit_backpack(str(id))
        return result

@app.route("/api/backpacks/create", methods=['POST'])
def create_backpack_api():
    """Create a new backpack
    ---
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#definitions/Backpack'
    responses:
      '200':
        description: It was not an backpack
      '201':
        description: A new backpack was created
    """
    if request.method == "POST":
        backpack = request.get_json()
        new_backpack = create_backpack(backpack)
        if type(new_backpack) is backpack:
            response = jsonify()
            response.status_code = 201
            response.headers['location'] = '/api/backpacks/' + backpack.id
            emit_backpack(str(backpack.id))
            return response
        #return jsonify(create_backpack(backpack))


@app.route("/api/backpacks/<id>/create", methods=['PUT'])
def create_backpack_put_api(id):
    """Create a new backpack
    ---
    parameters:
      - name: id
        in: path
        type: integer
        required: true
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#definitions/Backpack'
    responses:
      '200':
        description: It was not an backpack
      '201':
        description: A new backpack was created
    """
    if request.method == "PUT":
        backpack = request.get_json()
        backpack["id"] = id
        new_backpack = create_backpack(backpack)
        if type(new_backpack) is backpack:
            response = jsonify()
            response.status_code = 201
            response.headers['location'] = '/api/backpacks/' + backpack.id
            emit_backpack(str(backpack.id))
            return response

@app.route("/api/backpacks/<id>/delete", methods=['DELETE'])
def delete_backpack_api(id):
    """Delete a backpack
    ---
    parameters:
      - name: id
        in: path
        type: string
        required: true
    responses:
      '204':
        description: Backpack was deleted
      '404':
        description: Backpack not found
    """
    if request.method == "DELETE":
        backpack = Backpack.query.filter_by(id=id).first_or_404()
        db.session.delete(backpack)
        db.session.commit()
        emit_backpack(str(backpack.id))
        return "204"

### Items
###

@app.route("/api/items")
def show_all_items():
    """Route to list all items
    ---
    responses:
      '200':
        description: A list of items
        schema:
            type: array
            items:
                $ref: '#/definitions/Item'
    definitions:
        Item:
            type: object
            required:
                - id
                - name
                - type_id
                - backpack_id
            properties:
                id:
                    type: integer
                    example: 1337
                name:
                    type: string
                    example: "Banana 14"
                expiration_date:
                    type: string
                    example: "2021-01-03"
                type_id:
                    type: integer
                    example: 1337
                backpack_id:
                    type: integer
                    example: 1337
                count:
                    type: integer
                    example: 1
    """
    it = db.session.query(Item, ItemType.id, ItemType.name).join(ItemType, ItemType.id == Item.type_id).all()

    items = []
    for item in it:
        items.append({
            **json.loads(json.dumps(item[0]), object_hook=date_hook),
            "type_id": item[1],
            "type_name": item[2],
        })

    return jsonify(items)

@app.route("/api/items/<id>/update", methods=['PATCH'])
def update_item_api(id):
    """Update a specific item
    ---
    parameters:
      - name: id
        in: path
        type: string
        required: true
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#definitions/Item'
    responses:
      '200':
        description: A single item
        schema:
            $ref: '#/definitions/Item'
      '404':
        description: Item not found
    """
    if request.method == "PATCH":
        item = request.get_json()
        item['id'] = id
        result = jsonify(update_item(item))
        emit_backpack(str(item['backpack_id']))
        return result

@app.route("/api/items/create", methods=['POST'])
def create_item_api():
    """Create a new item
    ---
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#definitions/Item'
    responses:
      '200':
        description: It was not an item
      '201':
        description: A new item was created
    """
    if request.method == "POST":
        item = request.get_json()
        new_item = create_item(item)
        if type(new_item) is Item:
            response = jsonify()
            response.status_code = 201
            response.headers['location'] = '/api/items/' + item.id
            return response
        #return jsonify(create_item(item))

@app.route("/api/items/<id>/create", methods=['PUT'])
def create_item_put_api(id):
    """Create a new item
    ---
    parameters:
      - name: id
        in: path
        type: string
        required: true
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#definitions/Item'
    responses:
      '200':
        description: It was not an item
      '201':
        description: A new item was created
    """
    if request.method == "PUT":
        item = request.get_json()
        item["id"] = id
        new_item = create_item(item)
        if type(new_item) is Item:
            response = jsonify()
            response.status_code = 201
            response.headers['location'] = '/api/items/' + item.id
            return response

@app.route("/api/items/<id>/delete", methods=['DELETE'])
def delete_item_api(id):
    """Delete an item
    ---
    parameters:
      - name: id
        in: path
        type: string
        required: true
    responses:
      '204':
        description: Item was deleted
      '404':
        description: Item not found
    """
    if request.method == "DELETE":
        item = Item.query.filter_by(id=id).first_or_404()
        db.session.delete(item)
        db.session.commit()
        emit_backpack(str(item.backpack_id))
        return "204"

@app.route("/api/item_types")
def show_all_item_types():
    """Route to list all item types
    ---
    responses:
      '200':
        description: A list of item types
        schema:
            type: array
            items:
                $ref: '#/definitions/ItemType'
    definitions:
        ItemType:
            type: object
            required:
                - id
                - name
                - items
            properties:
                id:
                    type: integer
                    example: 1337
                name:
                    type: string
                    example: "Bananas"
                items:
                    type: array
                    items:
                        $ref: '#/definitions/Item'
    """
    its = ItemType.query.all()
    return jsonify(its)


# Fillter routes for basic socket testing
@app.route("/api/properties/status")
def status():
    """Route to get global status setting
    ---
    responses:
      '200':
        description: Get setting
        schema:
            $ref: '#/definitions/Settings'
    definitions:
        Settings:
            type: object
            required:
                - status
            properties:
                status:
                    type: boolean
    """
    global lamp_status
    return {"status": lamp_status}


def mutateStatus(b: bool):
    global lamp_status
    lamp_status = b
    socketio.emit("events", {"status": lamp_status})
    return ("", 204)


@app.route("/api/actions/switchOn", methods=["POST"])
def switchOn():
    """Route to get global status setting to true
    ---
    responses:
      '204':
        description: Status setting turned on
    """
    return mutateStatus(True)


@app.route("/api/actions/switchOff", methods=["POST"])
def switchOff():
    """Route to get global status setting to false
    ---
    responses:
      '204':
        description: Status setting turned off
    """
    return mutateStatus(False)

### MQTT
###

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")
    else:
        print("Failed to connect, return code %d\n", rc)
    mqtt.subscribe(MQTT_TOPIC)

@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    msg_decode = str(message.payload.decode("utf-8","ignore"))
    app.logger.info(msg_decode)
    try: 
      backpack = json.loads(msg_decode)
      update_backpack(backpack)
    except BaseException as err:
      app.logger.info("error")
      app.logger.info(err)
      pass
#    socketio.emit('mqtt_message', data=data)

@mqtt.on_log()
def handle_logging(client, userdata, level, buf):
    print("log", client, userdata, level, buf)

if __name__ == "__main__":
    #Reloader should be false in prod? https://flask-mqtt.readthedocs.io/en/latest/index.html#reloader
    socketio.run(app, host=FLASK_RUN_HOST, port=FLASK_RUN_PORT, use_reloader=USE_RELOADER)
