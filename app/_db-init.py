from flask import Flask
import os
from database import db, Users, Item, ItemType, Backpack, RoleEnum
from datetime import datetime

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["DATABASE_URL"]
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)

def seed():
  # Seeding users
  for i, name in enumerate(['Chris', 'Eetu', 'Jore', 'Lorenzo']):
    user = Users.query.filter_by(id=i).first()
    if (user is None):
      new_user = Users(i, name, RoleEnum.AMBULANCE if i % 2 == 0 else RoleEnum.HOSPITAL)
      db.session.add(new_user)

  db.session.commit()

  BACKPACK_COUNT = 3

  # Seeding backpacks
  for i in range(BACKPACK_COUNT):
    bp = Backpack.query.filter_by(id=str(i)).first()
    if (bp is None):
      backpack = Backpack(str(i), 'Backpack '+str(i), [])
      db.session.add(backpack)

  db.session.commit()

  # Seeding item types
  for i, name in enumerate(['Banana', 'Needle', 'Blood bag']):
    item_type = ItemType.query.filter_by(id=str(i)).first()
    if (item_type is None):
      new_item_type = ItemType(str(i), name, 1)
      db.session.add(new_item_type)

  db.session.commit()

  # Seeding item
  for i, name in enumerate(['Banana 1', 'Needle 1', 'Needle 2', 'Needle 3']):
    item = Item.query.filter_by(id=str(i)).first()
    if (item is None):
      item_type = ItemType.query.filter_by(name=name.split(' ')[0]).first()
      new_item = Item(str(i), name, datetime(2021, 11, 10), item_type.id, str(i % BACKPACK_COUNT))
      db.session.add(new_item)

  db.session.commit()

if __name__ == "__main__":
  with app.app_context():
    db.create_all()
    print("created All!")
    seed()
